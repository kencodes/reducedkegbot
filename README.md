# README #

Reduced Kegbot is a fork of an earlier Nuvation project, Kegbot. It will run on a Raspberry Pi and read flow meters that are connected to our beer taps. Its sole purpose is to report keg volumes by sending daily update emails to the admins.


### What is this repository for? ###

* All source files for the Reduced Kegbot project
* Version 0.1
* This project targets a Raspberry Pi (1) Model B

### How do I get set up? ###

* You will need a Pi, LAN connection (WiFi dongle), flow meter hardware interface board, source code and associated libraries, etc.
* (TBD) Configuration
* (TBD) Dependencies
* (TBD) Database configuration
* (TBD) How to run tests
* (TBD) Deployment instructions

### Contribution guidelines ###

Suggestions and contributions are very welcome.  I would appreciate hearing about anything you see wrong with the project or anything that could be improved.

* (TBD) Writing tests
* (TBD) Code review
* (TBD) Other guidelines

### Who do I talk to? ###

* Repo owner or admin (me)
* Visit blog.Nuvation.com to read about our earlier Kegbot project, which included electronic payments, SQL databases of users, beers, and taps, access control using solenoidal valves and which had a pretty web interface.