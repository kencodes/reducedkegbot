#!/usr/bin/env python2


import os
import MySQLdb
import yaml

os.chdir('/home/pi')

kegbot_config = open("/home/pi/kegbot_config.yaml", 'r')
kegbot_config_yaml = yaml.load(kegbot_config)
kegbot_config.close()

sql_host = '192.168.201.184'
sql_user = 'pi'
sql_passwd = 'kegbot'
sql_schema = 'test'
sql_df_users_table = 'df_users'

open_schema = MySQLdb.connect(sql_host, sql_user, sql_passwd, sql_schema)
open_schema.autocommit(True)
kegbot_cursor = open_schema.cursor()


try:
    while True:
        # Prompt for swipe
        first_name = ''
        last_name = ''
        camp = ''
        email = ''
        rfid = raw_input("Swipe your RFID tag: ")
        
        # Look for RFID tag # in database
        try:
            kegbot_cursor.execute("SELECT * FROM %s.%s WHERE rfid=%s" % (sql_schema, sql_df_users_table, rfid))
        except:
            print "There was an error"
        sql_search_results = kegbot_cursor.fetchall()
        
        # If more than 1 instance of this tag exists in database, something bad happened.
        if len(sql_search_results) > 1:
            print "Please tell Ken there is a database problem with this RFID tag"
            
        # If RFID tag already exists in database, ask if they want to change their info
        elif len(sql_search_results) == 1:
            print "Your RFID tag is already in the database."
            choice = raw_input("Would you like to update your information? [y/n]")
            if choice.lower() == 'n':
                print "okay"
            elif choice.lower() == 'y':
                print "Let's update your information\n"
                first_name = raw_input("Type your first name: ")
                last_name = raw_input("Type your last name: ")
                camp = 'asdf'
                while not (camp == 'DF' or camp == 'LP' or camp == ''):
                    camp = raw_input("What camp are you in? Enter DF, LP or just hit enter for neither: ")
                    if camp == '':
                        camp = 'UNKNOWN'
                        break
                    elif camp == 'DF':
                        break
                    elif camp == 'LP':
                        break
                email = raw_input("Enter your email address or just hit enter to finish registering: ")
                try:
                    # Delete existing info for this RFID tag
                    kegbot_cursor.execute("""DELETE FROM %s WHERE rfid=%s;""" % (sql_df_users_table, rfid))
                    
                    # Re-insert row with their info
                    kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`first_name`, `last_name`, `camp`, `rfid`, `email`)
                    VALUES ('%s', '%s', '%s', '%s', '%s');""" % (sql_schema, sql_df_users_table, first_name, last_name, camp, rfid, email))
                except:
                    print "There was an error"
                print "\nThank you for registering.\nYou will now be able to drink cold beer on the Playa!\n"
        # If tag is not in database, ask if they want to enter any info
        elif len(sql_search_results) == 0:
            print "This tag will be added to the database.\nEntering your info will help us better plan beer next year.\n"
            choice = raw_input("Would you like to enter your name for wacky statistics tracking?[y/n]")
            if choice.lower() == 'n':
                print "okay"
                # Enter their RFID # into the database, but don't include any personal info
                kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`rfid`)
                    VALUES ('%s');""" % (sql_schema, sql_df_users_table, rfid))
            elif choice.lower() == 'y':
                print "Let's update your information\n"
                first_name = raw_input("Type your first name: ")
                last_name = raw_input("Type your last name: ")
                camp = 'asdf'
                while not (camp == 'DF' or camp == 'LP' or camp == ''):
                    camp = raw_input("What camp are you in? Enter DF, LP or just hit enter for neither: ")
                    if camp == '':
                        camp = 'UNKNOWN'
                        break
                    elif camp == 'DF':
                        break
                    elif camp == 'LP':
                        break
                email = raw_input("Enter your email address or just hit enter to finish registering: ")
                try:
                    kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`first_name`, `last_name`, `camp`, `rfid`, `email`)
                    VALUES ('%s', '%s', '%s', '%s', '%s');""" % (sql_schema, sql_df_users_table, first_name, last_name, camp, rfid, email))
                except:
                    print "There was an error"
                print "\nThank you for registering.\nYou will now be able to drink cold beer on the Playa!\n"
            
            
                
                
                
except KeyboardInterrupt:
    if open_schema.open:
        try:
            print "Keyboard interrupt - Closing SQL Table"
            kegbot_cursor.close()
            open_schema.close()
        except:
            print "ERROR CLOSING SQL CURSOR/SCHEMA"